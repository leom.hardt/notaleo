package notaleo;


import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Aluno
 */
public class CalibrarPesosController implements Initializable {

    
    @FXML
    private TextField nTrab, nProva1, nProva2;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        nTrab.setText(""+NotaLeo.getPeso(0));
        nProva1.setText(""+NotaLeo.getPeso(1));
        nProva2.setText(""+NotaLeo.getPeso(2));
        
    }
    @FXML
    public void onSalvarClicked(){
        double[] pesos = new double[3];
        pesos[0] = Double.parseDouble(nTrab.getText());
        pesos[1] = Double.parseDouble(nProva1.getText());
        pesos[2] = Double.parseDouble(nProva2.getText());
        NotaLeo.setPesos(pesos);
    }
    @FXML
    public void onVoltarClicked(){
        NotaLeo.voltarTela("TelaInicial.fxml");
    }
    
}
