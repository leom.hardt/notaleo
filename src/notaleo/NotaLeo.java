/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notaleo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import notaleo.modelo.Aluno;

/**
 *
 * @author Aluno
 */
public class NotaLeo extends Application {
    
    private static Stage stage;
    
    private static Map<String, Scene> telas;
    
    private static double pesos[];
    
    ArrayList<Aluno> alunos;
    
    public static void voltarTela(String nomeDaTela){
        Scene tela = telas.get(nomeDaTela);
        if(tela == null){
            inicializarTela(nomeDaTela);
        } 
        stage.setScene(tela);
        stage.show();
        
    }
    
    public static void trocarTela(String nomeDaTela){
        telas.remove(nomeDaTela);
        inicializarTela(nomeDaTela);
        stage.setScene((telas.get(nomeDaTela)));
        stage.show();
    }
    
    private static void inicializarTela(String nomeDaTela){
        try{
            Parent root = FXMLLoader.load(NotaLeo.class.getResource(nomeDaTela));
            telas.put(nomeDaTela, new Scene(root));
        } catch(IOException e){
            System.out.println("Erro! " + e);
        }
        
    }
    
    @Override
    public void start(Stage stage) throws Exception {
        this.pesos = new double[3];
        this.alunos = new ArrayList<>();
        this.stage = stage;
        this.telas = new HashMap<>();
        inicializarTela("TelaInicial.fxml");
        
        trocarTela("TelaInicial.fxml");
    }
    
    public static void setPesos(double pesos[]){
        NotaLeo.pesos = pesos;
    }
    
    public static double getPeso(int i){
        return pesos[i];
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
