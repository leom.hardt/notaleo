/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notaleo;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import notaleo.modelo.Aluno;
/**
 *
 * @author Aluno
 */
public class TelaInicialController implements Initializable {

    @FXML
    private TableView tabela;
    
    @FXML
    private TableColumn colNome, colHistorico, colProva1, colProva2, colNTrab;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
    }
    
    private void tableAddAluno(Aluno a) {
        colNome.setCellValueFactory(new PropertyValueFactory<>("nome"));
        colHistorico.setCellValueFactory(new PropertyValueFactory<>("historico"));
        colProva1.setCellValueFactory(new PropertyValueFactory<>("prova1"));
        colProva2.setCellValueFactory(new PropertyValueFactory<>("prova2"));
        colNTrab.setCellValueFactory(new PropertyValueFactory<>("ntrab"));
    }
  
    
    @FXML
    public void onCadastrarAlunoClicked(){
        NotaLeo.trocarTela("CadastrarAluno.fxml");
    }
    
    @FXML
    public void onCalibrarPesosClicked(){
        NotaLeo.trocarTela("CalibrarPesos.fxml");
    }
    
    @FXML
    public void onDetalharAlunoClicked(){
        NotaLeo.trocarTela("DetalharAluno.fxml");
    }
}
