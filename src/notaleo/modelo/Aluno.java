/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notaleo.modelo;

import notaleo.NotaLeo;

/**
 *
 * @author Aluno
 */
public class Aluno {
    String nome;

    double notas[];
    double historico[];
    
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double[] getNotas() {
        return notas;
    }

    public void setNotas(double[] notas) {
        this.notas = notas;
    }
    
    public void setNota(int trimestre, double nota){
        notas[trimestre] = nota;
    }


    public void setHistorico(double[] historico) {
        this.historico = historico;
    }
    
    public void setNotaHistorico(int trimestre, double nota){
        this.historico[trimestre] = nota;
    }
    
    public double getMedia(double p1, double p2, double p3){
        return (p1 + p2 + p3)/(p1/notas[1] + p2/notas[2] + p3/notas[3]);
    }
    
    public double getProva1(){
        return notas[1];
    }
    public double getTrab(){
        return notas[0];
    }
    public double getProva2(){
        return notas[2];
    }
    public double getHistorico(){
        double p1 = NotaLeo.getPeso(0), p2 = NotaLeo.getPeso(1), p3 = NotaLeo.getPeso(2); 
        return (p1 + p2 + p3)/(p1/notas[1] + p2/notas[2] + p3/notas[3]);
    }
}
